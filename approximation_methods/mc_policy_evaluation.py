import numpy as np
import matplotlib.pyplot as plt
from dynamic_programming.grid import standard_grid, Grid, print_policy, print_values

# NOTE: this is only policy evaluation, not optimization

# we'll try to obtain the same result as our other MC script
from monte_carlo.policy_evaluation import play_game

LEARNING_RATE = 0.001

if __name__ == '__main__':

    grid = standard_grid()
    print_values(grid.rewards, grid, 'Rewards:')

    # found by policy_iteration_random on standard_grid
    # MC method won't get exactly this, but should be close
    # values:
    # ---------------------------
    #  0.43|  0.56|  0.72|  0.00|
    # ---------------------------
    #  0.33|  0.00|  0.21|  0.00|
    # ---------------------------
    #  0.25|  0.18|  0.11| -0.17|
    # policy:
    # ---------------------------
    #   R  |   R  |   R  |      |
    # ---------------------------
    #   U  |      |   U  |      |
    # ---------------------------
    #   U  |   L  |   U  |   L  |

    policy = Grid.winning_policy
    theta = np.random.randn(4) / 2

    def state_to_features(s):
        return np.array([s[0] - 1, s[1] - 1.5, s[0] * s[1] - 3, 1])

    # repeat until convergence
    deltas = []
    t = 1.0
    for iteration in range(20000):
        if iteration % 100 == 0:
            t += 0.01
        alpha = LEARNING_RATE / t
        # generate an episode using pi
        delta = 0
        states_and_returns = play_game(grid, policy, gamma=0.9, windy=True)
        seen_states = set()
        for s, G in states_and_returns:
            if s not in seen_states:
                old_theta = theta.copy()
                x = state_to_features(s)
                V_hat = theta.dot(x)
                # grad(V_hat) wrt theta = x
                theta += alpha * (G - V_hat) * x
                delta = max(delta, np.abs(old_theta - theta).sum())
                seen_states.add(s)
        deltas.append(delta)

    plt.plot(np.convolve(deltas, np.ones((100,)) / 100, mode='valid'))
    plt.show()

    V = {}
    states = grid.all_states()
    for s in states:
        if s in grid.actions:
            V[s] = theta.dot(state_to_features(s))
        else:
            V[s] = 0

    print_values(V, grid, 'Values:')
    print_policy(policy, grid, 'Policy')
    print(theta)
