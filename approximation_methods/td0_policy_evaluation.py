import numpy as np
import matplotlib.pyplot as plt
from dynamic_programming.grid import standard_grid, print_values, print_policy, Grid
from temporal_difference.td0_prediction import play_game


class Model:
    def __init__(self):
        self.theta = np.random.randn(4) / 2

    @staticmethod
    def state_to_features(s):
        return np.array([s[0] - 1, s[1] - 1.5, s[0] * s[1] - 3, 1])

    def predict(self, s):
        x = self.state_to_features(s)
        return self.theta.dot(x)

    def grad(self, s):
        return self.state_to_features(s)


if __name__ == '__main__':
    grid = standard_grid()
    print_values(grid.rewards, grid, 'Rewards:')
    policy = Grid.winning_policy
    model = Model()
    deltas = []
    ALPHA = 0.1
    GAMMA = 0.9

    k = 1.0
    for it in range(20000):
        if it % 10 == 0:
            k += 0.01
        alpha = ALPHA / k
        biggest_change = 0
        states_and_rewards = play_game(grid, policy)
        for t in range(len(states_and_rewards) - 1):
            s, _ = states_and_rewards[t]
            s2, r = states_and_rewards[t + 1]
            old_theta = model.theta.copy()
            if grid.is_terminal(s2):
                target = r
            else:
                target = r + GAMMA * model.predict(s2)
            model.theta += alpha * (target - model.predict(s)) * model.grad(s)
            biggest_change = max(biggest_change, np.abs(old_theta - model.theta).sum())
        deltas.append(biggest_change)

    plt.plot(deltas)
    plt.show()

    # obtain predicted values
    V = {}
    states = grid.all_states()
    for s in states:
        if s in grid.actions:
            V[s] = model.predict(s)
        else:
            # terminal state or state we can't otherwise get to
            V[s] = 0

    print_values(V, grid, 'Values')
    print_policy(policy, grid, 'Policy')
