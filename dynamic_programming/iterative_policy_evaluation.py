"""
iterative policy evaluation
Given a policy, let's find it's value function V(s)
We will do this for both a uniform random policy and fixed policy
NOTE:
there are 2 sources of randomness
p(a|s) - deciding what action to take given the state
p(s',r|s,a) - the next state and reward given your action-state pair
we are only modeling p(a|s) = uniform
how would the code change if p(s',r|s,a) is not deterministic?
"""

import numpy as np
from dynamic_programming.grid import standard_grid, print_policy, print_values

THRESHOLD = 1e-3  # threshold for convergence

if __name__ == '__main__':

    # Uniform Random Policy
    grid = standard_grid()
    states = grid.all_states()
    gamma = 1.0  # Discount Factor
    V = {s: 0 for s in states}  # Initialize Value Function to 0
    while True:
        delta = 0
        for s in states:
            old_v = V[s]

            if s in grid.actions:
                new_v = 0
                p_a = 1.0 / len(grid.actions[s])  # Same probability to transition to any state
                for a in grid.actions[s]:
                    grid.set_state(s)
                    reward = grid.move(a)
                    new_v += p_a * (reward + gamma * V[grid.current_state()])
                V[s] = new_v
                delta = max(delta, np.abs(old_v - V[s]))
        if delta < THRESHOLD:
            break
    print("values for uniformly random actions:")
    print_values(V, grid)
    print("\n\n")

    # Fixed policy
    policy = {(2, 0): 'U', (1, 0): 'U', (0, 0): 'R', (0, 1): 'R', (0, 2): 'R',
              (1, 2): 'R', (2, 1): 'R', (2, 2): 'R', (2, 3): 'U'}

    print_policy(policy, grid)
    V = {s: 0 for s in states}
    gamma = 0.9

    while True:
        delta = 0
        for s in states:
            old_v = V[s]
            if s in policy:
                a = policy[s]
                grid.set_state(s)
                reward = grid.move(a)
                V[s] = reward + gamma * V[grid.current_state()]
                delta = max(delta, np.abs(old_v - V[s]))

        if delta < THRESHOLD:
            break
    print("values for fixed policy:")
    print_values(V, grid)
