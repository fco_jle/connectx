import numpy as np
from connectx.dynamic_programming.grid import negative_grid
from connectx.dynamic_programming.iterative_policy_evaluation import print_values, print_policy


def policy_evaluation(policy, states, value_function, grid, convergence_threshold=1e-3, gamma=0.9):
    while True:
        delta = 0
        for state in states:
            old_value = value_function[state]
            if state in policy.keys():
                action = policy[state]
                grid.set_state(state)
                reward = grid.move(action)
                value_function[state] = reward + gamma * value_function[grid.current_state()]
                delta = max(delta, np.abs(old_value - value_function[state]))
        if delta < convergence_threshold:
            break
    print_values(value_function, grid)
    return value_function


def policy_improvement(states, policy, grid, value_function, gamma=0.9):
    policy_converged = True
    for state in states:
        if state in policy.keys():
            old_action = policy[state]
            new_action = None
            best_value = float('-inf')
            for action in ('U', 'D', 'L', 'R'):
                grid.set_state(state)
                reward = grid.move(action)
                v = reward + gamma * value_function[grid.current_state()]
                if v > best_value:
                    best_value = v
                    new_action = action
            policy[state] = new_action
            if new_action != old_action:
                policy_converged = False
    return policy, policy_converged


if __name__ == '__main__':
    """
    This grid gives you a reward of -0.1 for every non-terminal state
    we want to see if this will encourage finding a shorter path to the goal.
    
    Policy in initialized with random actions, Value function with random values. 
    """
    g = negative_grid()
    s = g.all_states()

    p = {s: np.random.choice(('U', 'D', 'L', 'R')) for s in g.actions.keys()}
    V = {s: (np.random.random() if s in g.actions else 0) for _ in s}

    print_values(g.rewards, g)
    print("initial policy:")
    print_policy(p, g)

    done = False
    while not done:  # repeat until convergence
        V = policy_evaluation(p, s, V, g, convergence_threshold=1e-3, gamma=0.9)
        p, done = policy_improvement(s, p, g, V, gamma=0.9)

    print_values(V, g)
    print_policy(p, g)
