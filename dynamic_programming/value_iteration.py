import numpy as np
from .grid import negative_grid, print_values, print_policy

SMALL_ENOUGH = 1e-3
GAMMA = 0.9
ALL_POSSIBLE_ACTIONS = ('U', 'D', 'L', 'R')

if __name__ == '__main__':

    # Initialize policy and value function
    grid = negative_grid()
    states = grid.all_states()
    policy = {s: np.random.choice(ALL_POSSIBLE_ACTIONS) for s in grid.actions.keys()}
    V = {s: np.random.random() if s in grid.actions else 0 for s in states}

    # repeat until convergence
    # V[s] = max[a]{ sum[s',r] { p(s',r|s,a)[r + gamma*V[s']] } }
    while True:
        delta = 0
        for s in states:
            old_v = V[s]
            if s in policy:
                new_v = float('-inf')
                for a in ALL_POSSIBLE_ACTIONS:
                    grid.set_state(s)
                    r = grid.move(a)
                    v = r + GAMMA * V[grid.current_state()]
                    if v > new_v:
                        new_v = v
                V[s] = new_v
                delta = max(delta, np.abs(old_v - V[s]))

        if delta < SMALL_ENOUGH:
            break

    for s in policy.keys():
        best_a = None
        best_value = float('-inf')
        for a in ALL_POSSIBLE_ACTIONS:
            grid.set_state(s)
            r = grid.move(a)
            v = r + GAMMA * V[grid.current_state()]
            if v > best_value:
                best_value = v
                best_a = a
        policy[s] = best_a

    # our goal here is to verify that we get the same answer as with policy iteration
    print_values(V, grid)
    print_policy(policy, grid)
