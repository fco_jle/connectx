import numpy as np
import matplotlib.pyplot as plt
from dynamic_programming.grid import Grid, negative_grid, print_policy, print_values
from tqdm import tqdm


def play_game(grid, policy, gamma=0.9):
    # Select one position at random and position the agent in that state on the grid:
    start_states = list(grid.actions.keys())
    start_idx = np.random.choice(len(start_states))
    grid.set_state(start_states[start_idx])

    # Get the state and select a random action to start with
    state = grid.current_state()
    action = np.random.choice(Grid.possible_actions)

    # Initialize some variables:
    states_actions_rewards = [(state, action, 0)]
    seen_states = set()
    seen_states.add(grid.current_state())
    num_steps = 0
    while True:
        reward = grid.move(action)
        num_steps += 1
        state = grid.current_state()

        if state in seen_states:
            reward = -10. / num_steps
            states_actions_rewards.append((state, None, reward))
            break
        elif grid.game_over():
            states_actions_rewards.append((state, None, reward))
            break
        else:
            action = policy[state]
            states_actions_rewards.append((state, action, reward))
        seen_states.add(state)

    return_g = 0
    states_actions_returns = []
    first = True
    for state, action, reward in reversed(states_actions_rewards):
        if first:
            first = False
        else:
            states_actions_returns.append((state, action, return_g))
        return_g = reward + gamma * return_g
    states_actions_returns.reverse()
    return states_actions_returns


def max_dict(d):
    max_key = max(d, key=d.get)
    return max_key, d[max_key]


def initialize_variables(grid):
    q = {}
    ret = {}
    sts = grid.all_states()
    for state in sts:
        if state in g.actions:
            q[state] = {}
            for action in Grid.possible_actions:
                q[state][action] = 0
                ret[(state, action)] = []
        else:
            pass
    return q, ret, sts, {}, []


if __name__ == '__main__':
    # Initialization
    g = negative_grid(step_cost=-0.9)
    p = {s: np.random.choice(Grid.possible_actions) for s in g.actions.keys()}
    Q, returns, states, V, deltas = initialize_variables(g)

    for t in tqdm(range(1000)):
        delta = 0
        s_a_r = play_game(g, p)
        seen_state_action_pairs = set()
        for s, a, G in s_a_r:
            sa = (s, a)
            if sa not in seen_state_action_pairs:
                old_q = Q[s][a]
                returns[sa].append(G)
                Q[s][a] = np.mean(returns[sa])
                delta = max(delta, np.abs(old_q - Q[s][a]))
                seen_state_action_pairs.add(sa)
        deltas.append(delta)

        for s in p.keys():
            p[s] = max_dict(Q[s])[0]

    for s in Q.keys():
        V[s] = max_dict(Q[s])[1]

    # Plots and Prints
    plt.plot(deltas)
    plt.show()
    print_values(g.rewards, g, 'Rewards:')
    print_policy(p, g, 'Final Policy:')
    print_values(V, g, "Final values:")
