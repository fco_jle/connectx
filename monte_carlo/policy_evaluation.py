import numpy as np
from dynamic_programming.grid import standard_grid, print_values, print_policy, Grid


def random_action(a):
    r = np.random.random()
    return a if r < 0.5 else np.random.choice([action for action in Grid.possible_actions if action != a])


def play_game(grid, policy, gamma=0.9, windy=False):
    start_states = list(grid.actions.keys())
    start_idx = np.random.choice(len(start_states))
    grid.set_state(start_states[start_idx])

    state = grid.current_state()
    states_and_rewards = [(state, 0)]  # list of tuples of (state, reward)
    while not grid.game_over():
        action = policy[state]
        if windy:
            action = random_action(action)
        reward = grid.move(action)
        state = grid.current_state()
        states_and_rewards.append((state, reward))

    return_g = 0
    states_and_returns = []
    first = True
    for state, reward in reversed(states_and_rewards):
        if first:
            first = False
        else:
            states_and_returns.append((state, return_g))
        return_g = reward + gamma * return_g
    states_and_returns.reverse()  # we want it to be in order of state visited
    return states_and_returns


if __name__ == '__main__':
    g = standard_grid()
    states = g.all_states()
    p = g.winning_policy

    # Evaluate Normal Grid World
    V = {s: 0 for s in states if s not in g.actions}
    returns = {s: [] for s in states if s in g.actions}
    for _ in range(5000):
        states_returns = play_game(g, p, gamma=0.9, windy=False)
        seen_states = set()
        for s, G in states_returns:
            if s not in seen_states:
                returns[s].append(G)
                V[s] = np.mean(returns[s])
                seen_states.add(s)
    print('Normal GridWorld:')
    print_values(V, g, 'Value Function')

    # Evaluate Windy Grid World
    V = {s: 0 for s in states if s not in g.actions}
    returns = {s: [] for s in states if s in g.actions}
    for _ in range(5000):
        states_returns = play_game(g, p, gamma=0.9, windy=True)
        seen_states = set()
        for s, G in states_returns:
            if s not in seen_states:
                returns[s].append(G)
                V[s] = np.mean(returns[s])
                seen_states.add(s)
    print('Windy GridWorld:')
    print_values(V, g, 'Value Function')

    # Print additional data:
    print_values(g.rewards, g, 'Rewards:')
    print_policy(p, g, 'Policy:')
