from .bandit import Bandit
from .epsilon_greedy import epsilon_greedy
from .bayesian import bayesian
from .upper_confidence_bound import upper_confidence_bound

__all__ = ['Bandit',
           'epsilon_greedy',
           'bayesian',
           'upper_confidence_bound']
