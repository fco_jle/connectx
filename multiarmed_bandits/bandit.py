import numpy as np


class Bandit:
    def __init__(self, m, initial_mean=0):
        self.m = m
        self.mean = initial_mean
        self.N = 0

        # Parameters for Bayesian Bandits (mu - prior is N(0, 1))
        self.m0 = initial_mean
        self.lambda0 = 1
        self.sum_x = 0
        self.tau = 1

    def pull(self):
        return np.random.randn() + self.m

    def sample(self):
        return np.random.randn() / np.sqrt(self.lambda0) + self.m0

    def update(self, x):
        self.N += 1
        self.mean = (1 - 1.0 / self.N) * self.mean + 1.0 / self.N * x

        # Update parameters of bayesian bandit
        self.lambda0 += 1
        self.sum_x += x
        self.m0 = self.tau*self.sum_x / self.lambda0
