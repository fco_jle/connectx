import numpy as np
import matplotlib.pyplot as plt


def bayesian(bandits, steps):
    data = np.empty(steps)
    for i in range(steps):
        choice = np.argmax([b.sample() for b in bandits])
        x = bandits[choice].pull()
        bandits[choice].update(x)
        data[i] = x

    cumulative_average = np.cumsum(data) / (np.arange(steps) + 1)
    return cumulative_average


if __name__ == '__main__':
    from multiarmed_bandits import Bandit

    levers = [Bandit(1), Bandit(2), Bandit(3)]
    bound = bayesian(levers, 100000)

    # Log scale Plot
    plt.plot(bound, label='bayesian')
    plt.legend()
    plt.xscale('log')
    plt.show()
    plt.grid()