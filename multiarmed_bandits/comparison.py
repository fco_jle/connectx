if __name__ == '__main__':
    import multiarmed_bandits
    import matplotlib.pyplot as plt

    normal_bandits = [multiarmed_bandits.Bandit(1),
                      multiarmed_bandits.Bandit(2),
                      multiarmed_bandits.Bandit(3)]

    opt_bandits = [multiarmed_bandits.Bandit(1, initial_mean=10),
                   multiarmed_bandits.Bandit(2, initial_mean=10),
                   multiarmed_bandits.Bandit(3, initial_mean=10)]

    greedy1 = multiarmed_bandits.epsilon_greedy(bandits=normal_bandits, epsilon=0.1, steps=100000)
    greedy2 = multiarmed_bandits.epsilon_greedy(bandits=normal_bandits, epsilon=0.05, steps=100000)
    opt1 = multiarmed_bandits.epsilon_greedy(bandits=opt_bandits, epsilon=0.1, steps=100000)
    opt2 = multiarmed_bandits.epsilon_greedy(bandits=opt_bandits, epsilon=0.05, steps=100000)
    ucb = multiarmed_bandits.upper_confidence_bound(bandits=normal_bandits, steps=100000)
    bayes = multiarmed_bandits.bayesian(bandits=normal_bandits, steps=100000)

    plt.plot(greedy1, label='Greedy 0.1')
    plt.plot(greedy2, label='Greedy 0.05')
    plt.plot(opt1, label='Opt 0.1')
    plt.plot(opt2, label='Opt 0.05')
    plt.plot(ucb, label='UCB')
    plt.plot(bayes, label='Bayesian')
    plt.legend()
    plt.xscale('log')
    plt.grid()
    plt.show()
