import numpy as np
import matplotlib.pyplot as plt
from multiarmed_bandits import Bandit


def epsilon_greedy(bandits, epsilon, epsilon_decay=False, min_epsilon=0.005, steps=1000):
    data = np.empty(steps)

    for i in range(steps):
        choose_at_random = np.random.random() < epsilon
        if choose_at_random:
            choice = np.random.choice(len(bandits))
        else:
            choice = np.argmax([b.mean for b in bandits])
        x = bandits[choice].pull()
        bandits[choice].update(x)
        if epsilon_decay and epsilon > min_epsilon:
            epsilon = epsilon * (1 / (i+1))
            epsilon = max(min_epsilon, epsilon)
        data[i] = x

    cumulative_average = np.cumsum(data) / (np.arange(steps) + 1)
    return cumulative_average


if __name__ == '__main__':
    levers = [Bandit(1), Bandit(2), Bandit(3)]

    eps_1 = epsilon_greedy(bandits=levers, epsilon=1, epsilon_decay=True, steps=100000)
    eps_2 = epsilon_greedy(bandits=levers, epsilon=0.1, steps=100000)
    eps_3 = epsilon_greedy(bandits=levers, epsilon=0.05, steps=100000)
    eps_4 = epsilon_greedy(bandits=levers, epsilon=0.01, steps=100000)
    eps_5 = epsilon_greedy(bandits=levers, epsilon=0.002, steps=100000)

    plt.plot(np.arange(100000), eps_1, label='E=1, with decay')
    plt.plot(np.arange(100000), eps_2, label='E=0.1 no decay')
    plt.plot(np.arange(100000), eps_3, label='E=0.05 no decay')
    plt.plot(np.arange(100000), eps_4, label='E=0.01 no decay')
    plt.plot(np.arange(100000), eps_5, label='E=0.002 no decay')
    plt.xscale('log')
    plt.legend()
    plt.grid()
    print('Done!')
