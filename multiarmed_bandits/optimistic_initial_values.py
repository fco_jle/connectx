import matplotlib.pyplot as plt
from multiarmed_bandits import Bandit


if __name__ == '__main__':
    from multiarmed_bandits.epsilon_greedy import epsilon_greedy

    levers = [Bandit(1, initial_mean=10), Bandit(2, initial_mean=10), Bandit(3, initial_mean=10)]
    optimistic = epsilon_greedy(levers, epsilon=0.1, steps=100000)

    plt.plot(optimistic, label='Optimistic')
    plt.legend()
    plt.xscale('log')
    plt.show()
