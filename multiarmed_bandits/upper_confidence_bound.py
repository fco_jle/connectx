import numpy as np
import matplotlib.pyplot as plt
from multiarmed_bandits import Bandit


def calculate_ucb(mean, total_steps, bandit_steps):
    if bandit_steps == 0:
        val = float('inf')
    else:
        val = mean + np.sqrt(2 * np.log(total_steps) / bandit_steps)
    return val


def upper_confidence_bound(bandits, steps):
    data = np.empty(steps)
    for i in range(steps):
        ucb_values = [calculate_ucb(b.mean, i + 1, b.N) for b in bandits]
        choice = np.argmax(ucb_values)
        x = bandits[choice].pull()
        bandits[choice].update(x)
        data[i] = x
    cumulative_average = np.cumsum(data) / (np.arange(steps) + 1)
    return cumulative_average


if __name__ == '__main__':

    levers = [Bandit(1), Bandit(2), Bandit(3)]
    bound = upper_confidence_bound(levers, 100000)

    # Log scale Plot
    plt.plot(bound, label='ucb')
    plt.legend()
    plt.xscale('log')
    plt.show()
    plt.grid()
