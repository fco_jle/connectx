
import random


def random_agent(observation, configuration):
    legal_actions = [c for c in range(configuration.columns) if observation.board[c] == 0]
    selected_action = random.choice(legal_actions)
    return selected_action

