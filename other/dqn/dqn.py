import gym
from kaggle_environments import make
import tensorflow as tf


class ConnectX(gym.Env):
    def __init__(self):
        self.env = make('connectx', debug=False)
        self.columns = self.env.configuration.get('columns')
        self.rows = self.env.configuration.get('rows')
        self.action_space = gym.spaces.Discrete(self.columns)
        self.observation_space = gym.spaces.Discrete(self.columns * self.rows)

    def step(self, actions):
        return self.env.step(actions)

    def reset(self):
        return self.env.reset()

    def render(self, **kwargs):
        self.env.render(**kwargs)

    @property
    def state(self):
        return self.env.state

    @property
    def done(self):
        return self.env.done

    @property
    def current_player(self):
        return 0 if self.env.state[0]['status'] == 'ACTIVE' else 1

    @property
    def configuration(self):
        return self.env.configuration


class ConnectNet(tf.keras.Model):
    def __init__(self, num_states, hidden_units, num_actions):
        super(ConnectNet, self).__init__()
        self.input_layer = tf.keras.layers.InputLayer(input_shape=(num_states,))
        self.hidden_layers = []
        for i in hidden_units:
            self.hidden_layers.append(tf.keras.layers.Dense(i, activation='relu', kernel_initializer='he_normal'))
        self.output_layer = tf.keras.layers.Dense(num_actions, activation='sigmoid', kernel_initializer='RandomNormal')

    @tf.function
    def call(self, inputs):
        z = self.input_layer(inputs)
        for layer in self.hidden_layers:
            z = layer(z)
        output = self.output_layer(z)
        return output
