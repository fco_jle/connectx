from tensorflow import function
from tensorflow.keras import Model
from tensorflow.keras.layers import Dense, InputLayer


class DeepModel(Model):
    def __init__(self, num_states, num_actions, hidden_layer_units):
        super(DeepModel, self).__init__()
        self.input_layer = InputLayer(input_shape=(num_states,))
        self.hidden_layers = []
        for i in hidden_layer_units:
            self.hidden_layers.append(Dense(i, activation='sigmoid', kernel_initializer='RandomNormal'))
        self.output_layer = Dense(num_actions, activation='linear', kernel_initializer='RandomNormal')

    @function
    def call(self, inputs):
        z = self.input_layer(inputs)
        for layer in self.hidden_layers:
            z = layer(z)
        output = self.output_layer(z)
        return output
