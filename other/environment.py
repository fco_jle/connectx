import gym
from kaggle_environments import make


class ConnectX(gym.Env):
    def __init__(self, adversary='random'):
        super(ConnectX, self).__init__()
        self.env = make('connectx', debug=True)
        self.pair = [None, adversary]
        self.trainer = self.env.train(self.pair)
        self.action_space = gym.spaces.Discrete(self.configuration.columns)
        self.observation_space = gym.spaces.Discrete(self.configuration.columns * self.configuration.rows)

    @property
    def configuration(self):
        return self.env.configuration

    @property
    def state(self):
        return self.env.state

    @property
    def done(self):
        return self.env.done

    @property
    def current_player(self):
        return 0 if self.env.state[0]['status'] == 'ACTIVE' else 1

    def render(self, **kwargs):
        return self.env.render(**kwargs)

    def reset(self):
        return self.env.reset()

    def step(self, actions):
        self.env.step(actions=actions)
