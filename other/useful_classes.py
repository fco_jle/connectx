import gym
import pickle
import random
import numpy as np
from enum import Enum
from kaggle_environments import make


# Reward Enum
class Reward(Enum):
    Win = 10
    Draw = 0
    Loss = -10
    Cont = -0.5

    @staticmethod
    def transform(done, reward):
        if reward == 1:
            return Reward.Win.value
        elif reward == 0:
            return Reward.Loss.value
        else:
            if done:
                return Reward.Draw.value
            else:
                return Reward.Cont.value


# Environment Class
class ConnectX(gym.Env):
    env = make('connectx', debug=True)

    def __init__(self, adversary='random'):
        self.players = [None, adversary]
        self.trainer = self.env.train(self.players)
        self.switch_prob = 0.5
        self.config = self.env.configuration
        self.action_space = gym.spaces.Discrete(self.config.columns)
        self.observation_space = gym.spaces.Discrete(self.config.columns * self.config.rows)

    def switch_players(self):
        self.players = self.players[::-1]
        self.trainer = self.env.train(self.players)

    def step(self, action):
        return self.trainer.step(action)

    def reset(self, switch=False):
        if switch:
            self.switch_players()
        return self.trainer.reset()

    def render(self, **kwargs):
        return self.env.render(**kwargs)


# Agent Class (Decision Making)
class Agent:
    def __init__(self):
        self.actions = ConnectX.action_space.n
        self.game_steps = []
        self.rewards = []
        self.epsilons = []
        self.alphas = []

    def random(self, s):
        selection = random.choice([c for c in range(self.actions) if s.board[c] == 0])
        return selection

    def q_learning(self, q_table, state):
        q_state = q_table(state)[:]
        q_values = [q_state[j] if state.board[j] == 0 else float("-inf") for j in range(self.actions)]
        selection = int(np.argmax(q_values))
        return selection

    def update_history(self, game_length, final_reward, epsilon, alpha):
        self.game_steps.append(game_length)
        self.rewards.append(final_reward)
        self.epsilons.append(epsilon)
        self.alphas.append(alpha)


# Q-Table Class, store states and q-values
class QTable:
    def __init__(self, columns=7):
        self.table = dict()
        self.action_space = gym.spaces.Discrete(columns)

    def __call__(self, state):
        state_key = self.encode(state)
        if state_key not in self.table.keys():
            self.add_item(state_key)
        return self.table[state_key]

    @staticmethod
    def encode(state):
        board = state.board[:].copy()
        board.append(state.mark)
        state_key = int(''.join([str(c) for c in board]))
        state_key = hex(state_key)
        return state_key

    @staticmethod
    def decode(state_key):
        state_orig = [int(c) for c in str(int(state_key, 16)).zfill(43)]
        orig_board = state_orig[:-1]
        orig_mark = state_orig[-1]
        return orig_board, orig_mark

    def add_item(self, state_key):
        self.table[state_key] = list(np.zeros(self.action_space.n))

    def clean_table(self):
        self.table = {k: v for k, v in self.table.items() if np.count_nonzero(v) > 0}

    def save_table(self, filename):
        with open(filename, 'wb') as fp:
            pickle.dump(self.table, fp, protocol=pickle.HIGHEST_PROTOCOL)

    def load_table(self, filename):
        with open(filename, 'rb') as fp:
            self.table = pickle.load(fp)

    def update(self, observation, action, q_value):
        self.table[self.encode(observation)][action] = q_value


# Class to store Q-Learning parameters
class LearningParams:
    def __init__(self):
        self.a = 0.8  # 𝛼 defines how strongly a new value will affect the average
        self.g = 0.6  # 𝛾 discount factor, The lower 𝛾, the more we value immediate rewards
        self.epsilon = 1  # ε, probability that we take a random action in the simulation
        self.min_epsilon = 0.1  # Minimum probability for a random step
        self.episodes = 10  # Number of simulations to run
        self.alpha_decay_step = 1000  # Number of steps in which alpha parameter will be lowered
        self.alpha_decay_rate = 0.98  # Ratio for the new alpha after alpha_decay_step steps
        self.epsilon_decay_rate = 0.9998  # Ratio of decay for epsilon

    def update_epsilon(self):
        self.epsilon = max(self.min_epsilon, self.epsilon * self.epsilon_decay_rate)

    def update_alpha(self, step):
        if (step + 1) % self.alpha_decay_step == 0:
            self.a = self.a * self.alpha_decay_rate


class QAgent:
    @staticmethod
    def action(q_table, observation, configuration):
        state_key = QTable.encode(observation)
        if state_key not in q_table.keys():
            return QAgent.random_action(observation, configuration)
        else:
            return QAgent.q_action(q_table[state_key], observation, configuration)

    @staticmethod
    def q_action(q_state, observation, configuration):
        not_null_actions = np.asarray([1 if observation.board[c] == 0 else 0 for c in range(configuration.columns)])
        q_values = np.asarray(q_state)
        fixed_q = []
        for i, q in enumerate(q_values):
            if not_null_actions[i] == 1:
                fixed_q.append(q_values[i])
            else:
                fixed_q.append(-1e6)
        action = int(np.argmax(fixed_q))
        return action

    @staticmethod
    def random_action(observation, configuration):
        action = random.choice([c for c in range(configuration.columns) if observation.board[c] == 0])
        return action


class QTrainer:
    def __init__(self, q_table=None, adversary='negamax'):
        self.env = ConnectX(adversary=adversary)
        self.table = QTable()
        if q_table:
            self.table.load_table(q_table)
        self.params = LearningParams()
        self.agent = QAgent()
        self.current_observation = None
        self.episode_rewards = []

    def reset_rewards(self):
        self.episode_rewards = []

    def q_episode(self):
        self.reset_rewards()
        current_observation = self.env.reset(switch=False)
        done = False

        while not done:
            if random.uniform(0, 1) < self.params.epsilon:
                action = self.agent.random_action(current_observation, self.env.config)
            else:
                action = self.agent.q_action(self.table(current_observation),
                                             current_observation,
                                             self.env.config)

            new_observation, reward, done, info = self.env.step(action)
            reward = Reward.transform(done, reward)
            self.episode_rewards.append(reward)
            q_value = self.q_rule(self.table(current_observation)[action], new_observation, reward)
            self.table.update(current_observation, action, q_value)
            current_observation = new_observation

    def q_rule(self, q0, q1_array, r):
        q_value = (1 - self.params.a) * q0 + self.params.a * (r + self.params.g * np.max(self.table(q1_array)))
        return q_value

    def update_table(self, observation, new_observation, action, reward):
        val = self.q_rule(self.table(observation)[action], new_observation, reward)
        self.table(observation)[action] = val

    def train(self, num_episodes=10):
        for i in range(num_episodes):
            self.q_episode()
            self.params.update_epsilon()
            self.table.clean_table()
            self.params.update_alpha(i+1)
        print('Finished Training')

    def save_table(self, filename='q_table.json'):
        self.table.save_table(filename)


if __name__ == '__main__':
    trainer = QTrainer()
    trainer.train(100)
    print('Episode Finished')
