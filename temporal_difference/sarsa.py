import numpy as np
import matplotlib.pyplot as plt
from dynamic_programming.grid import negative_grid, print_values, print_policy, Grid
from monte_carlo.exploring_starts import max_dict
from temporal_difference.td0_prediction import random_action

GAMMA = 0.9
ALPHA = 0.1
ALL_POSSIBLE_ACTIONS = Grid.possible_actions

if __name__ == '__main__':
    grid = negative_grid(step_cost=-0.1)
    print_values(grid.rewards, grid, "Rewards:")

    Q = {}
    states = grid.all_states()
    for s in states:
        Q[s] = {}
        for a in ALL_POSSIBLE_ACTIONS:
            Q[s][a] = 0

    # let's also keep track of how many times Q[s] has been updated
    update_counts = {}
    update_counts_sa = {}
    for s in states:
        update_counts_sa[s] = {}
        for a in ALL_POSSIBLE_ACTIONS:
            update_counts_sa[s][a] = 1.0

    # repeat until convergence
    t = 1.0
    deltas = []
    for it in range(10000):
        if it % 100 == 0:
            t += 1e-2
        if it % 2000 == 0:
            print("it:", it)

        s = (2, 0)  # start state
        grid.set_state(s)
        a = max_dict(Q[s])[0]
        a = random_action(a, eps=0.5 / t)
        biggest_change = 0
        while not grid.game_over():
            r = grid.move(a)
            s2 = grid.current_state()
            a2 = max_dict(Q[s2])[0]
            a2 = random_action(a2, eps=0.5 / t)  # epsilon-greedy
            alpha = ALPHA / update_counts_sa[s][a]
            update_counts_sa[s][a] += 0.005
            old_qsa = Q[s][a]
            Q[s][a] = Q[s][a] + alpha * (r + GAMMA * Q[s2][a2] - Q[s][a])
            biggest_change = max(biggest_change, np.abs(old_qsa - Q[s][a]))
            update_counts[s] = update_counts.get(s, 0) + 1
            s = s2
            a = a2
        deltas.append(biggest_change)

    plt.plot(deltas)
    plt.show()

    policy = {}
    V = {}
    for s in grid.actions.keys():
        a, max_q = max_dict(Q[s])
        policy[s] = a
        V[s] = max_q

    total = np.sum(list(update_counts.values()))
    for k, v in update_counts.items():
        update_counts[k] = float(v) / total

    print_values(update_counts, grid, "Update counts:")
    print_values(V, grid, 'Values:')
    print_policy(policy, grid, 'Policy')
