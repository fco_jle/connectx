import numpy as np
from dynamic_programming.grid import standard_grid, print_values, print_policy, Grid


def random_action(a, eps=0.1):
    if np.random.random() < eps:
        return np.random.choice(Grid.possible_actions)
    else:
        return a


def play_game(grid, policy):
    s = (2, 0)
    grid.set_state(s)
    states_and_rewards = [(s, 0)]  # list of tuples of (state, reward)
    while not grid.game_over():
        a = policy[s]
        a = random_action(a)
        r = grid.move(a)
        s = grid.current_state()
        states_and_rewards.append((s, r))
    return states_and_rewards


if __name__ == '__main__':
    GAMMA = 0.9
    ALPHA = 0.1
    g = standard_grid()
    pi = g.standard_policy
    V = {s: 0 for s in g.all_states()}

    for _ in range(1000):
        states_rewards = play_game(g, pi)
        for t in range(len(states_rewards) - 1):
            s, _ = states_rewards[t]
            s2, rw = states_rewards[t + 1]
            V[s] = V[s] + ALPHA * (rw + GAMMA * V[s2] - V[s])

    print_values(g.rewards, g, 'Rewards:')
    print_values(V, g, 'Values:')
    print_policy(pi, g, 'Policy:')
