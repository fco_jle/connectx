import numpy as np

LENGTH = 3


class Environment:
    def __init__(self):
        self.board = np.zeros((LENGTH, LENGTH))
        self.x = -1  # Player 1
        self.o = 1  # Player 2
        self.winner = None
        self.done = False
        self.num_states = 3 ** (LENGTH ** 2)

    def reset(self):
        self.board = np.zeros((LENGTH, LENGTH))
        self.winner = None
        self.done = False
        return self

    def is_empty(self, i, j):
        return self.board[i, j] == 0

    def reward(self, symbol):
        if not self.done:
            return 0
        else:
            if self.winner == symbol:
                return 1
            else:
                return 0

    def get_state(self):
        k = 0
        h = 0
        for i in range(LENGTH):
            for j in range(LENGTH):
                if self.board[i, j] == 0:
                    v = 0
                elif self.board[i, j] == self.x:
                    v = 1
                elif self.board[i, j] == self.o:
                    v = 2
                else:
                    raise ValueError('Board value at position is not possible')

                h += (3 ** k) * v
                k += 1
        return h

    def game_over(self, force_recalculate=False):
        if not force_recalculate and self.done:
            return self.done

        # Check Rows
        for i in range(LENGTH):
            for player in (self.x, self.o):
                if self.board[i].sum() == player * LENGTH:
                    self.winner = player
                    self.done = True
                    return True

        # Check Columns
        for i in range(LENGTH):
            for player in (self.x, self.o):
                if self.board[:, i].sum() == player * LENGTH:
                    self.winner = player
                    self.done = True
                    return True

        # Check Diagonals
        for player in (self.x, self.o):
            if self.board.trace() == player * LENGTH:
                self.winner = player
                self.done = True
                return True

            if np.fliplr(self.board).trace() == player * LENGTH:
                self.winner = player
                self.done = True
                return True

        # Check for Draw:
        if np.all(self.board != 0):
            self.winner = None
            self.done = True
            return True

        # Else game is not over
        self.winner = None
        return False

    def render(self):
        for i in range(LENGTH):
            print("-------------")
            for j in range(LENGTH):
                print("  ", end="")
                if self.board[i, j] == self.x:
                    print("x ", end="")
                elif self.board[i, j] == self.o:
                    print("o ", end="")
                else:
                    print("  ", end="")
            print("")
        print("-------------")


class Agent:
    def __init__(self, epsilon=0.1, alpha=0.5):
        self.eps = epsilon
        self.alpha = alpha
        self.verbose = False
        self.state_history = []
        self.sym = '-'
        self.V = None

    def set_v(self, v):
        self.__setattr__('V', v)

    def set_symbol(self, symbol):
        self.__setattr__('sym', symbol)

    def set_verbose(self, v):
        self.__setattr__('verbose', v)

    def reset_history(self):
        self.state_history = []

    def take_action(self, env):
        r = np.random.rand()
        if r < self.eps:
            if self.verbose:
                print("Taking a random action")

            possible_moves = []
            for i in range(LENGTH):
                for j in range(LENGTH):
                    if env.is_empty(i, j):
                        possible_moves.append((i, j))
            idx = np.random.choice(len(possible_moves))
            next_move = possible_moves[idx]
        else:
            pos2value = {}  # for debugging
            next_move = None
            best_value = -1
            for i in range(LENGTH):
                for j in range(LENGTH):
                    if env.is_empty(i, j):
                        env.board[i, j] = self.sym
                        state = env.get_state()
                        env.board[i, j] = 0
                        pos2value[(i, j)] = self.V[state]
                        if self.V[state] > best_value:
                            best_value = self.V[state]
                            next_move = (i, j)

            # if verbose, draw the board w/ the values
            if self.verbose:
                print("Taking a greedy action")
                for i in range(LENGTH):
                    print("------------------")
                    for j in range(LENGTH):
                        if env.is_empty(i, j):
                            # print the value
                            print(" %.2f|" % pos2value[(i, j)], end="")
                        else:
                            print("  ", end="")
                            if env.board[i, j] == env.x:
                                print("x  |", end="")
                            elif env.board[i, j] == env.o:
                                print("o  |", end="")
                            else:
                                print("   |", end="")
                    print("")
                print("------------------")

        env.board[next_move[0], next_move[1]] = self.sym

    def update_state_history(self, s):
        self.state_history.append(s)

    def update(self, env):
        assert isinstance(env, Environment)
        reward = env.reward(self.sym)
        target = reward
        for prev in reversed(self.state_history):
            value = self.V[prev] + self.alpha * (target - self.V[prev])
            self.V[prev] = value
            target = value
        self.reset_history()


class Human:
    def __init__(self):
        self.sym = '-'

    def set_symbol(self, symbol):
        self.sym = symbol

    def take_action(self, env):
        assert isinstance(env, Environment)
        while True:
            move = input('Enter coordinates i,j for your move:')
            i, j = move.split(',')
            i = int(i)
            j = int(j)
            if env.is_empty(i, j):
                env.board[i, j] = self.sym
                break

    def update(self, env):
        pass

    def update_state_history(self, env):
        pass


def play_game(p1, p2, env, draw=0):
    assert isinstance(env, Environment)
    if isinstance(p2, Human):
        print('Playing as human')
    # loops until the game is over
    current_player = None
    while not env.game_over():
        # alternate between players
        # p1 always starts first
        if current_player == p1:
            current_player = p2
        else:
            current_player = p1

        # draw the board before the user who wants to see it makes a move
        if draw:
            if draw == 1 and current_player == p1:
                env.render()
            if draw == 2 and current_player == p2:
                env.render()

        # current player makes a move
        current_player.take_action(env)

        # update state histories
        state = env.get_state()
        p1.update_state_history(state)
        p2.update_state_history(state)

    if draw:
        env.render()

    # do the value function update
    p1.update(env)
    p2.update(env)


def initial_v_x(env, state_winner_triples):
    # initialize state values as follows
    # if x wins, V(s) = 1
    # if x loses or draw, V(s) = 0
    # otherwise, V(s) = 0.5
    V = np.zeros(env.num_states)
    for state, winner, ended in state_winner_triples:
        if ended:
            if winner == env.x:
                v = 1
            else:
                v = 0
        else:
            v = 0.5
        V[state] = v
    return V


def initial_v_o(env, state_winner_triples):
    # this is (almost) the opposite of initial V for player x
    # since everywhere where x wins (1), o loses (0)
    # but a draw is still 0 for o
    V = np.zeros(env.num_states)
    for state, winner, ended in state_winner_triples:
        if ended:
            if winner == env.o:
                v = 1
            else:
                v = 0
        else:
            v = 0.5
        V[state] = v
    return V


def get_state_hash_and_winner(env, i=0, j=0):
    results = []

    for v in (0, env.x, env.o):
        env.board[i, j] = v  # if empty board it should already be 0
        if j == 2:
            # j goes back to 0, increase i, unless i = 2, then we are done
            if i == 2:
                # the board is full, collect results and return
                state = env.get_state()
                ended = env.game_over(force_recalculate=True)
                winner = env.winner
                results.append((state, winner, ended))
            else:
                results += get_state_hash_and_winner(env, i + 1, 0)
        else:
            # increment j, i stays the same
            results += get_state_hash_and_winner(env, i, j + 1)

    return results


if __name__ == '__main__':
    player1 = Agent()
    player2 = Agent()

    environment = Environment()
    st_wn_trips = get_state_hash_and_winner(environment)

    Vx = initial_v_x(environment, st_wn_trips)
    player1.set_v(Vx)
    Vo = initial_v_o(environment, st_wn_trips)
    player2.set_v(Vo)

    player1.set_symbol(environment.x)
    player2.set_symbol(environment.o)

    for t in range(10000):
        if t % 200 == 0:
            print(t)
        play_game(player1, player2, environment.reset())

    human = Human()
    human.set_symbol(environment.o)
    while True:
        player1.set_verbose(True)
        play_game(player1, human, Environment(), draw=2)
        answer = input('Play again? [Y/n]: ')
        if answer and answer.lower()[0] == 'n':
            break
